import pygame
import sys
from random import randint

WIDTH = 1280
HEIGHT = 800

RACKET_WIDTH = 20
RACKET_HEIGHT = 120
RACKET_SPEED = 5

BALL_RADIUS = 10
BALL_SPEED = 5
BALL_MAX_Y_SPEED = 5

BLACK = (0,0,0)
WHITE = (255, 255, 255)
FPS = 64

class Racket:
    def __init__(self, x, y):
        self.color = WHITE
        self.width = RACKET_WIDTH
        self.height = RACKET_HEIGHT
        self.speed = RACKET_SPEED
        self.x = x
        self.y = y

    def draw(self, display):
        pygame.draw.rect(display, self.color, (self.x, self.y, self.width, self.height))

    def move(self, up=True):
        if up and ((self.y - self.speed) > 0): 
            self.y -= self.speed
        elif ((self.y + self.speed) < (HEIGHT - self.height)):
            self.y += self.speed

class Ball:
    def __init__(self, left_x, right_x, y):
        self.color = WHITE
        self.radius = BALL_RADIUS
        self.speed_x = BALL_SPEED
        self.speed_y = randint(0, BALL_MAX_Y_SPEED)
        self.left_x = left_x
        self.right_x = right_x
        if randint(0, 1) == 0:
            self.x = left_x
        else:
            self.x = right_x
        self.y = y
        self.max_y_speed = BALL_MAX_Y_SPEED

    def reset(self, player, new_ball_y):
        self.speed_x = BALL_SPEED
        self.speed_y = randint(0, BALL_MAX_Y_SPEED)
        if player == "left":
            self.x = self.left_x
        else:
            self.x = self.right_x
        self.y = new_ball_y

    def draw(self, display):
        pygame.draw.circle(display, self.color, (self.x, self.y), self.radius)
    
    def update_ball_y_speed(self, racket):
        racket_middle_y = (racket.y + (racket.height // 2))
        d_y = self.y - racket_middle_y
        ball_y_speed = (self.max_y_speed * (d_y / (racket.height // 2)))
        self.speed_y = ball_y_speed

    def move(self, left_racket, right_racket):

        # Handle vertical collisions.
        if self.y <= self.radius:
            self.speed_y = -self.speed_y
        if self.y >= (HEIGHT - self.radius):
            self.speed_y = -self.speed_y

        # Handle vertical collisions and game reset.
        if (self.x + self.radius) >= WIDTH:
            self.reset("right", (right_racket.y + (right_racket.height // 2)))
        if (self.x - self.radius) <= 0:
            self.reset("left", (left_racket.y + (left_racket.height // 2))) 

        # Handle right racket collision.
        if (self.x + self.radius) >= right_racket.x:
            if (self.y > right_racket.y) and (self.y < (right_racket.y + right_racket.height)):
                # We're handling the right racket collision here.
                # Reverse the X speed.
                self.speed_x = - self.speed_x
                # Calculate the Y speed.
                self.update_ball_y_speed(right_racket)


        # Handle left racket collision.
        if (self.x - self.radius) <= (left_racket.x + left_racket.width):
            if (self.y > left_racket.y) and (self.y < (left_racket.y + left_racket.height)):
                # We're handling the left racket collision here.
                # Reverse the X speed.
                self.speed_x = - self.speed_x
                # Calculate the Y speed.
                self.update_ball_y_speed(left_racket)

        self.x += self.speed_x
        self.y += self.speed_y


def draw_all(display, rackets, ball):
    pygame.display.flip()
    display.fill(BLACK)
    for racket in rackets:
        racket.draw(display)
    ball.draw(display)

def main():
    pygame.init()
    display = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption("Pong")
    clock = pygame.time.Clock()

    left_racket = Racket(10, ((HEIGHT // 2) - (RACKET_HEIGHT // 2)))
    right_racket = Racket((WIDTH - RACKET_WIDTH - 10), ((HEIGHT // 2) - (RACKET_HEIGHT // 2)))


    left_x, right_x = (12 + RACKET_WIDTH + BALL_RADIUS), (WIDTH - 12 - RACKET_WIDTH - BALL_RADIUS)
    ball = Ball(left_x, right_x, (HEIGHT // 2))

    while True:
        clock.tick(FPS)
        draw_all(display, [left_racket, right_racket], ball)
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
                break
        
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            right_racket.move(False)
        if keys[pygame.K_RIGHT]:
            right_racket.move(True)
        if keys[pygame.K_a]:
            left_racket.move(False)
        if keys[pygame.K_d]:
            left_racket.move(True)
        
        ball.move(left_racket, right_racket)


if __name__ == '__main__':
    main()